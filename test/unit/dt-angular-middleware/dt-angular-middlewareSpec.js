'use strict';

describe('', function() {

  var module;
  var dependencies;
  dependencies = [];

  var hasModule = function(module) {
  return dependencies.indexOf(module) >= 0;
  };

  beforeEach(function() {

  // Get module
  module = angular.module('dt-angular-middleware');
  dependencies = module.requires;
  });

  it('should load config module', function() {
    expect(hasModule('dt-angular-middleware.config')).to.be.ok;
  });

  

  

  
  it('should load services module', function() {
    expect(hasModule('dt-angular-middleware.services')).to.be.ok;
  });
  

  
    it('should load controllers module', function() {
      expect(hasModule('dt-angular-middleware.controllers')).to.be.ok;
    });
  

});
