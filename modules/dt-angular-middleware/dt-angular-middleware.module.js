(function (angular) {

  // Create all modules and define dependencies to make sure they exist
  // and are loaded in the correct order to satisfy dependency injection
  // before all nested files are concatenated by Gulp

  // Config
  angular.module('dt-angular-middleware.config', [])
      .value('dt-angular-middleware.config', {
          debug: true
      });

  // Modules
  angular.module('dt-angular-middleware.services', []);
  angular.module('dt-angular-middleware.controllers', []);
  angular.module('dt-angular-middleware',
      [
        'dt-angular-middleware.config',
        'dt-angular-middleware.services',
        'dt-angular-middleware.controllers',
        'ngResource',
        'ngRoute',
        'ngCookies',
        'firebase'
      ]);

   angular.module('dt-angular-middleware').config(  MiddlewareConfig );

   angular.module('dt-angular-middleware').run(  [ function(){
      console.log('i am running');
   } ]);

})(angular);


MiddlewareConfig.$inject = [ 'MiddlewareProvider' ];

function MiddlewareConfig( MiddlewareProvider  )
{
   MiddlewareProvider.init();
    console.log('i am config running');
}
