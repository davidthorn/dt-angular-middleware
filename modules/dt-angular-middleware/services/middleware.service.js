/**
 * Created by coichedid on 21/04/15.
 */
'use strict';

angular.module('dt-angular-middleware').provider('Middleware',  MiddlewareProvider  );


MiddlewareProvider.$inject = [ '$routeProvider' ];

function MiddlewareProvider( $routeProvider  )
{


    this.init = function()
    {
        console.log('init my middleware');
    };

    this.$get = [ 'Authenication' ,  function( Authenication )
    {
        return Authenication;
    }];

}
